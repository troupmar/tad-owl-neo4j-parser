import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martin on 12.11.15.
 */
public class DataTriplet {
    private DataNode subject;
    private String relType;
    private DataNode object;

    public DataTriplet(DataNode subject, String relType, DataNode object) {
        this.subject = subject;
        this.relType = relType;
        this.object = object;
    }

    public DataNode getSubject() {
        return subject;
    }

    public String getRelType() {
        return relType;
    }

    public DataNode getObject() {
        return object;
    }

    public List<String> getSignatures() {
        List<String> signatures = new ArrayList<>();
        List<String> subjLabels = subject.getLabels();
        List<String> objLabels = object.getLabels();
        for (String subjLabel : subjLabels) {
            for (String objLabel : objLabels) {
                signatures.add("(" + subjLabel + ")-[" + relType + "]->(" + objLabel + ")");
            }
        }
        return signatures;
    }

    public String signatureToString() {
        return "(" + subject.labelsToString() + ")-[" + relType + "]->(" + object.labelsToString() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataTriplet triplet = (DataTriplet) o;

        if (subject != null ? !subject.getName().equals(triplet.getSubject().getName()) : triplet.getSubject() != null) return false;
        if (object != null ? !object.getName().equals(triplet.getObject().getName()) : triplet.getObject() != null) return false;
        if (relType != null ? !relType.equals(triplet.getRelType()) : triplet.getRelType() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = subject != null ? subject.hashCode() : 0;
        result = 31 * result + (relType != null ? relType.hashCode() : 0);
        result = 31 * result + (object != null ? object.hashCode() : 0);
        return result;
    }
}
