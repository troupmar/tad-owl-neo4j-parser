import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Martin on 12.11.15.
 */
public class Neo4jDatabase {
    private static Neo4jDatabase instance = null;

    private GraphDatabaseService database;

    private Neo4jDatabase() {
        File databaseDir = new File("database");
        deleteDir(databaseDir);
        database = new GraphDatabaseFactory().newEmbeddedDatabase(databaseDir);
    }

    public static Neo4jDatabase getInstance() {
        if(instance == null) {
            instance = new Neo4jDatabase();
        }
        return instance;
    }

    // Delete database directory if it exists
    private boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    // Get a label for a node - any label except of _META_ label
    public NodeLabels getRepreLabel(Node node) {
        Iterable<Label> nodeLabels;
        try (Transaction tx = database.beginTx()) {
            nodeLabels = node.getLabels();
            tx.success();
        }
        Iterator<Label> it = nodeLabels.iterator();
        while (it.hasNext()) {
            Label label = it.next();
            if (label.name().equals("_META_")) {
                continue;
            } else {
                return Mapper.getLabel(label.name());
            }
        }
        return null;
    }

    public Node createNode(DataNode dataNode) {
        Node node;
        try (Transaction tx = database.beginTx()) {
            node = createNodeWithLabels(dataNode.getLabels());
            if (dataNode.getName() != null) {
                node.setProperty("name", dataNode.getName());
            }
            for (Map.Entry<String, String> property : dataNode.getProperties().entrySet()) {
                node.setProperty(property.getKey(), property.getValue());
            }
            tx.success();
        }
        return node;
    }

    public Node createNode(DataClass dataClass) {
        Node node;
        try (Transaction tx = database.beginTx()) {
            node = createNodeWithLabels(dataClass.getLabels());
            tx.success();
        }
        return node;
    }

    private Node createNodeWithLabels(List<String> labels) {
        Node node;
        try (Transaction tx = database.beginTx()) {
            node = database.createNode(Mapper.getLabel(labels.get(0)));
            for (int i=1; i<labels.size(); i++) {
                node.addLabel(Mapper.getLabel(labels.get(i)));
            }
            tx.success();
        }
        return node;
    }

    public void createRelationship(DataTriplet dataTriplet) {
        try (Transaction tx = database.beginTx() ) {
            NodeLabels subjLabel = Mapper.getLabel(dataTriplet.getSubject().getPrimaryLabel());
            NodeLabels objLabel = Mapper.getLabel(dataTriplet.getObject().getPrimaryLabel());
            RelationshipTypes relType = Mapper.getRelationshipType(dataTriplet.getRelType());

            Node subject = database.findNode(subjLabel, "name", dataTriplet.getSubject().getName());
            Node object = database.findNode(objLabel, "name", dataTriplet.getObject().getName());

            subject.createRelationshipTo(object, relType);
            tx.success();
        }
    }

    public void createRelationship(Node subject, RelationshipTypes relType, Node object) {
        try (Transaction tx = database.beginTx()) {
            subject.createRelationshipTo(object, relType);
            tx.success();
        }
    }

    public List<String> getNodeLabels(Node node) {
        List<String> labelsList = new ArrayList<>();
        try (Transaction tx = database.beginTx()) {
            Iterator<Label> labels = node.getLabels().iterator();

            while (labels.hasNext()) {
                labelsList.add(labels.next().name());
            }
            tx.success();
        }
        return labelsList;
    }

}

