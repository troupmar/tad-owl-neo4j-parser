import org.neo4j.graphdb.Label;

/**
 * Created by Martin on 12.11.15.
 */
public enum NodeLabels implements Label {
    Resource,
    Institution,
    Field,
    Researcher,
    _META_,
    Thing,
    Staff,
    Professor,
    tata,
}
