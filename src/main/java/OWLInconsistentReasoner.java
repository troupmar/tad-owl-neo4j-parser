/**
 * Created by Martin on 11.11.15.
 */
public class OWLInconsistentReasoner extends Exception {

    // Parameter-less Constructor
    public OWLInconsistentReasoner() {}

    // Constructor that accepts a message
    public OWLInconsistentReasoner(String message)
    {
        super(message);
    }
}
