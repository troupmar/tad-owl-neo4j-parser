import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.*;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Martin on 11.11.15.
 */
public class Parser {
    private OWLOntology ontology;
    private OWLReasoner reasoner;

    public Parser() {
        InputStream owlFile = main.class.getResourceAsStream("/smart_cities_updated.owl");
        loadOntology(owlFile);
        createReasoner();
    }

    private void loadOntology(InputStream resource) {
        try {
            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
            ontology = manager.loadOntologyFromOntologyDocument(resource);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
        }
    }

    private void createReasoner() {
        try {
            reasoner = new Reasoner(ontology);
            if (!reasoner.isConsistent()) {
                throw new OWLInconsistentReasoner();
            }
        } catch (OWLInconsistentReasoner e) {
            e.printStackTrace();
        }
    }

    public OWLOntology getOntology() {
        return ontology;
    }

    public OWLReasoner getReasoner() {
        return reasoner;
    }

    public DataClass getRootIndividual() {
        List<String> labels = new ArrayList<>();
        labels.add("Thing");
        return new DataClass(labels, labels.get(0));
    }

    public HashMap<String, DataClass> getClasses() {
        HashMap<String, DataClass> classes = new HashMap<>();
        // Go over classes in owl file
        for (OWLClass owlClass : ontology.getClassesInSignature()) {
            // Get class name
            String className = getNameFromURI(owlClass.toString());
            // Create new DataClass instance
            DataClass dataClass = new DataClass(className, className);
            // Save it
            classes.put(className, dataClass);
        }
        for (OWLClass owlClass : ontology.getClassesInSignature()) {
            String className = getNameFromURI(owlClass.toString());
            // Get superclasses of a class
            Object[] superClasses = owlClass.getSuperClasses(ontology).toArray();
            // If a class has a superclass -> set appropriate superclass for a class
            if (superClasses.length > 0) {
                String superClassName = getNameFromURI(superClasses[0].toString());
                classes.get(className).setSuperClass(classes.get(superClassName));
            }
        }
        return classes;
    }

    public List<DataNode> getIndividuals() {
        List<DataNode> individuals = new ArrayList<>();
        // Get classes
        HashMap<String, DataClass> classes = getClasses();

        for (OWLClass owlClass : ontology.getClassesInSignature()) {
            // Get current class
            String className = getNameFromURI(owlClass.toString());
            // Loop over all individuals belonging to current class
            for (Node<OWLNamedIndividual> individual : reasoner.getInstances(owlClass, true)) {
                // Get individual
                OWLNamedIndividual repreIndividual = individual.getRepresentativeElement();
                // Get individual's name
                String individualName = getNameFromURI(repreIndividual.toString());
                // Create a new DataNode for individual, set name + class + all inherited classes
                DataNode node = new DataNode(individualName, classes.get(className).getInheritedClasses(), className);
                // Loop over all existing data properties
                for (OWLDataProperty dataProperty : ontology.getDataPropertiesInSignature()) {
                    // Get value of a property for a specific individual
                    for (OWLLiteral literal : reasoner.getDataPropertyValues(repreIndividual, dataProperty)) {
                        // get the property name
                        String propertyName = dataProperty.toString();
                        propertyName = getNameFromURI(propertyName);
                        node.addProperty(propertyName, literal.getLiteral());
                        break;
                    }
                }
                Integer existingDataNodePosition = getExistingDataNodePosition(individuals, node);
                if (existingDataNodePosition != null) {
                    individuals.get(existingDataNodePosition).addLabel(className);
                    individuals.get(existingDataNodePosition).addPrimaryLabel(className);
                } else {
                    individuals.add(node);
                }
            }
        }
        return individuals;
    }


    public List<DataTriplet> getTriplets() {
        List<DataTriplet> triplets = new ArrayList<>();

        List<DataNode> individuals = getIndividuals();
        for (OWLClass owlClass : ontology.getClassesInSignature()) {
            //String subjClassName = getNameFromURI(owlClass.toString());

            for (Node<OWLNamedIndividual> individual : reasoner.getInstances(owlClass, true)) {
                // Get subject
                OWLNamedIndividual subject = individual.getRepresentativeElement();
                String subjectName = getNameFromURI(subject.toString());

                // Loop over all existing relationships
                for (OWLObjectProperty objectProperty : ontology.getObjectPropertiesInSignature()) {
                    // For a single relationship from a single subject find all objects (cause subject can have more relationships
                    // of the same type
                    for (Node<OWLNamedIndividual> object : reasoner.getObjectPropertyValues(subject, objectProperty)) {
                        // Get the name of the relationship
                        String relType = objectProperty.toString();
                        relType = getNameFromURI(relType);
                        // Get the name of the object
                        String objectName = object.getRepresentativeElement().toString();
                        objectName = getNameFromURI(objectName);
                        // Get object's class
                        //String objClassName = //getFirstPrimaryClassForIndividual(dataNodes, objectName);
                        // Create subject
                        DataNode subjDataNode = new DataNode(subjectName);
                        // Get existing subject -> it will have all inherited classes
                        subjDataNode = getExistingDataNode(individuals, subjDataNode);
                        // Create object
                        DataNode objDataNode = new DataNode(objectName);
                        // Get existing object -> it will have all inherited classes
                        objDataNode = getExistingDataNode(individuals, objDataNode);
                        // Create triplet
                        DataTriplet triplet = new DataTriplet(subjDataNode, relType, objDataNode);
                        // Create only if does not exist -> ie. A node with two classes and a relationship would be created
                        // multiple times since looping over classes
                        if (getExistingDataTriplet(triplets, triplet) == null) {
                            triplets.add(triplet);
                        }
                    }
                }
            }
        }
        return triplets;
    }

    private DataTriplet getExistingDataTriplet(List<DataTriplet> haystack, DataTriplet needle) {
        for (DataTriplet dataTriplet : haystack) {
            if (dataTriplet.equals(needle)) {
                return dataTriplet;
            }
        }
        return null;
    }

    private Integer getExistingDataNodePosition(List<DataNode> haystack, DataNode needle) {
        for (int i=0; i<haystack.size(); i++) {
            if (haystack.get(i).equals(needle)) {
                return i;
            }
        }
        return null;
    }

    private DataNode getExistingDataNode(List<DataNode> haystack, DataNode needle) {
        Integer position = getExistingDataNodePosition(haystack, needle);
        if (position == null) {
            return null;
        }
        return haystack.get(position);
    }

    private String getNameFromURI(String uri) {
        if (uri.contains("#")) {
            return uri.substring(uri.indexOf("#") + 1, uri.lastIndexOf(">"));
        }
        return uri;
    }

}
