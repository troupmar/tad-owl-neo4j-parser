import org.neo4j.graphdb.RelationshipType;

/**
 * Created by Martin on 12.11.15.
 */
public enum RelationshipTypes implements RelationshipType {
    isA,
    isFromInstitution,
    expertiseUsedBy,
    hasContributor,
    hasExpert,
    isDimensionOf,
    usesExpertiseFrom,
    isPartOf,
    hasPart,
    hasApplication,
    hasDimension,
    isAnImplementationOf,
    isAnApplicationOf,
    isExpertIn,
    canContributeTo,
}
