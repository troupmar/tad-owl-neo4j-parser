import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Martin on 25.11.15.
 */
public class Transformer {
    private Parser parser;
    private Neo4jDatabase neo4jDatabase;

    private Node rootNode;
    private DataClass rootDataClass;
    private HashMap<String, Node> classNodes;

    public Transformer() {
        parser = new Parser();
        neo4jDatabase = Neo4jDatabase.getInstance();
    }

    public void createMetaRootNode() {
        System.out.println("CREATING META ROOT THING NODE");
        rootDataClass = parser.getRootIndividual();
        rootNode = neo4jDatabase.createNode(rootDataClass);
        System.out.println("Created root [labels: " + rootDataClass.labelsToString() + "]");
        System.out.println();
    }

    public void createMetaClassesNodes() {
        System.out.println("CREATING META NODES FOR CLASSES");

        HashMap<String, DataClass> dataClasses = parser.getClasses();
        classNodes = new HashMap<>();

        for (DataClass dataClass : dataClasses.values()) {
            Node classNode = neo4jDatabase.createNode(dataClass);
            System.out.println("Created meta node [labels: " + dataClass.labelsToString() + "]");

            if (dataClass.getSuperClass() == null) {
                // Attach meta class node to the root node
                neo4jDatabase.createRelationship(classNode, RelationshipTypes.isA, rootNode);
                System.out.println("Created relationship (" + dataClass.getPrimaryLabel() + ")-[" +
                        RelationshipTypes.isA + "]->(" + rootDataClass.labelsToString() + ")");
            }
            classNodes.put(dataClass.getPrimaryLabel(), classNode);
        }
        for (DataClass dataClass : dataClasses.values()) {
            if (dataClass.getSuperClass() != null) {
                neo4jDatabase.createRelationship(classNodes.get(dataClass.getPrimaryLabel()), RelationshipTypes.isA,
                        classNodes.get(dataClass.getSuperClass().getPrimaryLabel()));
                System.out.println("Created relationship (" + dataClass.getPrimaryLabel() + ")-[" +
                        RelationshipTypes.isA + "]->(" + dataClass.getSuperClass().getPrimaryLabel() + ")");
            }
        }
        System.out.println();
    }

    public void createIndividualNodes() {
        System.out.println("CREATING INDIVIDUAL NODES");


        List<DataNode> individuals = parser.getIndividuals();
        for (DataNode individual : individuals) {
            Node nodeIndividual = neo4jDatabase.createNode(individual);
            System.out.println("Created individual [labels: " + individual.labelsToString() +
                    " name: " + individual.getName() + ", " + "properties: " + individual.propertiesToString() + "]");

            // Attach individual node to corresponding class nodes (each individual can have multiple labels = multiple classes)
            List<String> primaryLabels = individual.getPrimaryLabels();
            for (String primaryLabel : primaryLabels) {
                if (classNodes.containsKey(primaryLabel)) {
                    Node belongingClass = classNodes.get(primaryLabel);
                    neo4jDatabase.createRelationship(nodeIndividual, RelationshipTypes.isA, belongingClass);
                    System.out.println("Created relationship (" + individual.primaryLabelsToString() + ")-[" +
                            RelationshipTypes.isA + "]->(" + primaryLabel + ")");
                }
            }
        }

        System.out.println();
    }

    public void createRelationships() {
        System.out.println("CREATING RELATIONSHIPS");
        List<DataTriplet> triplets = parser.getTriplets();
        Ruler ruler = new Ruler();
        for (DataTriplet triplet : triplets) {
            if (ruler.isRuleAllowed(triplet.getSignatures())) {
                System.out.println("Rule allowed: " + triplet.signatureToString());
                neo4jDatabase.createRelationship(triplet);
            } else {
                System.out.println("Rule NOT allowed: " + triplet.signatureToString());
            }
        }
        System.out.println();
        System.out.println("Transformation finished!");
    }


}
