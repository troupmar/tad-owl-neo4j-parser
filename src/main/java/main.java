/**
 * Created by Martin on 11.11.15.
 */
public class main {
    public static void main(String[] args) {
        Transformer transformer = new Transformer();
        transformer.createMetaRootNode();
        transformer.createMetaClassesNodes();
        transformer.createIndividualNodes();
        transformer.createRelationships();
    }
}